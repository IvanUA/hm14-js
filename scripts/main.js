 // Підтримка збереження обраної теми в localStorage
function saveTheme(theme) {
    localStorage.setItem('selectedTheme', theme);
  }
  
  // Завантаження обраної теми з localStorage, якщо вона була збережена раніше
  function loadTheme() {
    return localStorage.getItem('selectedTheme') || 'light';
  }
  
  // Змінити тему
  function toggleTheme() {
    const body = document.body;
    const currentTheme = body.classList.contains('light-theme') ? 'light' : 'dark';
    const newTheme = currentTheme === 'light' ? 'dark' : 'light';
  
    body.classList.remove(currentTheme + '-theme');
    body.classList.add(newTheme + '-theme');
  
    saveTheme(newTheme);
  }
  
  // При кліку на кнопку "Змінити тему" викликається функція toggleTheme()
  document.getElementById('theme-toggle').addEventListener('click', toggleTheme);
  
  // Завантаження обраної теми при завантаженні сторінки
  document.addEventListener('DOMContentLoaded', function () {
    const savedTheme = loadTheme();
    document.body.classList.add(savedTheme + '-theme');
  });